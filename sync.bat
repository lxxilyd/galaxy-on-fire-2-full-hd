@ECHO OFF

set LOCAL_PATH="%USERPROFILE%\AppData\Roaming\Galaxy on Fire 2 Full HD\"
SET YYYY=%DATE:~0,4%
SET MM=%DATE:~5,2%
SET DD=%DATE:~8,2%
SET HH=%TIME:~0,2%
SET MI=%TIME:~3,2%
SET SS=%TIME:~6,2%

set /p opt=请选择操作: 1.从远程同步 2.同步到远程

if "%opt%"=="1" (
	call :syncFromRemote
) else if "%opt%"=="2" (
	call :syncToRemote
) else (
	echo 操作无效 && GOTO :EOF
)
pause
GOTO :EOF

@REM 从远程同步
:syncFromRemote
echo 开始从远程同步...
git fetch && git pull

echo 开始更新本地存档 %LOCAL_PATH% ...
xcopy .\gof2_* %LOCAL_PATH% /e /y /h /r /q /s /exclude:syncignore.txt

GOTO :EOF

@REM 同步到远程
:syncToRemote

echo 开始从本地存档 %LOCAL_PATH% 同步...
xcopy %LOCAL_PATH%\gof2_* .\ /e /y /h /r /q /s
echo 开始同步到远程
git fetch && git pull
git add .
git commit -m "同步存档-%YYYY%%MM%%DD%%HH%%MI%%SS%"
git push

GOTO :EOF
