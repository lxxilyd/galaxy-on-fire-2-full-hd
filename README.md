# Galaxy on Fire 2 Full HD 存档

## 介绍：

自用存档

## 使用方法

同步命令使用 双击 sync.bat 根据提示输入对应的菜单数字

```shell 
请选择操作: 1.从远程同步 2.同步到远程2
开始从本地存档 "C:\Users\Administrator\AppData\Roaming\Galaxy on Fire 2 Full HD\" 同步...
本地存档路径: "C:\Users\Administrator\AppData\Roaming\Galaxy on Fire 2 Full HD\"
复制了 7 个文件
开始同步到远程
[main e7f978d] 同步存档-20240613123739
 4 files changed, 0 insertions(+), 0 deletions(-)
```

## 原理

使用git存储存档文件，bat脚本自动提交